// React.
import React, {useState} from "react";
import {Route} from "react-router-dom";
// Styling.
import "normalize.css";
import "./App.scss";
// Views.
import Video from "./views/Video/Video.jsx";
// Components.
//import Navibar from "./components/Navibar/Navibar.jsx";
// etc.


function App() {
	/*
		State.
	*/
	// Authentication.
	const [auth, setAuth] = useState({
		client_id: "54ydn8226dbqbl7jgi14r6rd83ggsm"
	});


	/*
		DOM.
	*/
	return (
		<div className="App">
			<div className="View">
				<Route exact path="/videos/:video_id">
					<Video auth={auth}/>
				</Route>
			</div>

			<div className="Navi">
			</div>
		</div>
	);
}

export default App;

// React.
import React, {useState, useEffect} from "react";
import {useParams} from "react-router-dom";
// Styling.
import "./Video.scss";
// Packages.
import axios from "axios";
import TwitchPlayer from "react-player/lib/players/Twitch";
import Chart from "chart.js";
import Hammer from "hammerjs";
import zoom from "chartjs-plugin-zoom";
import crosshair from "chartjs-plugin-crosshair";
// etc.

function Video(props) {
	/*
		State.
	*/
	const ctx = React.useRef(null);
	const player = React.useRef(null);
	const [chart, Set_chart] = useState();
	const [video_id, Set_video_id] = useState(useParams().video_id);
	const [video, Set_video] = useState();
	const [comments, Set_comments] = useState();
	const [emotes, Set_emotes] = useState();
	const [data, Set_data] = useState();


	/*
		Helper functions.
	*/
	// Create chart.
	function Create_Chart() {
		if (!chart) {
			let chart = new Chart(ctx.current, {
				type: "line",
				data: {
					datasets: [
						{
							data: []
						}
					]
				},
				plugins: [
					zoom,
					crosshair
				],
				options: {
					// Responsiveness.
					responsive: true,
					maintainAspectRatio: false,
					// Disable animations.
					animation: {
						duration: 0
					},
					hover: {
						animationDuration: 0
					},
					responsiveAnimationDuration: 0,
					// Hide legend.
					legend: {
						display: false
					},
					// Element config.
					elements: {
						point: {
							radius: 0,
							backgroundColor: "transparent",
							borderWidth: 0,
							borderColor: "transparent"
						},
						line: {
							tension: 0
						}
					},
					// Axis config.
					scales: {
						xAxes: [
							{
								type: "linear",
								gridLines: {
									display: true
								},
								ticks: {
									// General.
									callback: Format_Time,
									lineHeight: 1,
									// Cartesian.
									autoSkipPadding: 64,
									minRotation: 0,
									maxRotation: 0,
									// Linear.
									beginAtZero: true,
									stepSize: 10
								}
							}
						],
						yAxes: [
							{
								display: false,
								type: "linear",
								gridLines: {
									display: false
								},
								ticks: {
									// General.

									// Cartesian.

									// Linear.
									beginAtZero: true,
									stepSize: 1
								}
							}
						]
					},
					// Tooltip config.
					tooltips: {
						enabled: false,
						mode: "nearest",
						intersect: false,
						axis: "x"
					},
					// Plugin config.
					plugins: {
						zoom: {
							pan: {
								enabled: true,
								mode: "x",
								rangeMin: {
									x: 0
								},
								rangeMax: {
									x: 0
								},
								threshold: 0
							},
							zoom: {
								enabled: true,
								mode: "x",
								rangeMin: {
									x: 0
								},
								rangeMax: {
									x: 0
								},
								speed: 0.2
							}
						},
						crosshair: {
							line: {

							},
							zoom: {
								enabled: false
							}
						}
					}
				}
			});

			// Seek by clicking on the chart.
			chart.options.onClick = (e) => {
				if (player.current) {
					if (chart.chartArea.top <= e.offsetY && e.offsetY <= chart.chartArea.bottom && chart.chartArea.left <= e.offsetX && e.offsetX <= chart.chartArea.right) {
						let seconds = (e.offsetX - chart.chartArea.left)/(chart.chartArea.right - chart.chartArea.left)*(chart.scales["x-axis-0"].max - chart.scales["x-axis-0"].min) + chart.scales["x-axis-0"].min;
						seconds = Math.round(seconds);
						player.current.seekTo(seconds, "seconds");
					}
				}
			}

			Set_chart(chart);
		}
	};

	// Update chart.
	function Update_Chart() {
		if (chart) {
			if (video) {
				chart.options.plugins.zoom.pan.rangeMax.x = video.duration_seconds;
				chart.options.plugins.zoom.zoom.rangeMax.x = video.duration_seconds;
			}
			else {
				chart.options.plugins.zoom.pan.rangeMax.x = 0;
				chart.options.plugins.zoom.zoom.rangeMax.x = 0;
				chart.resetZoom();
			}

			chart.data = data;

			chart.update();
		}
	};

	// Get video.
	async function Get_Video() {
		if (video_id) {
			let video;

			let config = {
				baseURL: "https://api.twitch.tv/helix/",
				url: "videos",
				method: "get",
				headers: {
					"Client-ID": props.auth.client_id
				},
				params: {
					id: video_id
				}
			};
			video = (await axios.request(config)).data.data[0];
			video.duration_seconds = Unformat_Time(video.duration);

			let data = {
				datasets: [
					{
						data: [{x: 0, y: 0}, {x: video.duration_seconds, y: 0}]
					}
				]
			};

			Set_video(video);
			Set_data(data);

			if (chart) {
				chart.resetZoom();
			}
		}
		else {
			Set_video();
			Set_data({
				datasets: [
					{
						data: []
					}
				]
			});
		}
	}

	// Get video comments.
	async function Get_Comments() {
		if (video) {
			let comments = [];

			let config = {
				baseURL: "https://api.twitch.tv/v5/",
				url: `videos/${video.id}/comments`,
				method: "get",
				headers: {
					"Client-ID": props.auth.client_id
				},
				params: {}
			};

			// Progress in console.
			let duration_seconds = video.duration.split(RegExp(/\D/)).slice(0, -1);
			duration_seconds = duration_seconds.reduce((seconds, e, i, a) => seconds + parseInt(e, 10)*(Math.pow(60, a.length - 1 - i)), 0);

			do {
				let res = await axios.request(config);
				comments.push(...res.data.comments);
				config.params.cursor = res.data._next;

				// Progress in console.
				console.log(`Loading comments: ${Math.round(comments[comments.length - 1].content_offset_seconds/duration_seconds*100)}%`);

			} while (config.params.cursor);

			// Progress in console.
			console.log("Loading comments: 100%");

			Set_comments(comments);
		}
		else {
			Set_comments();
		}
	};

	// Get Twitch emotes.
	async function Get_Twitch_Emotes() {
		let emotes = {};

		// Find emotes in comments.
		for (const comment of comments) {
			// Find emote fragments.
			for (const fragment of comment.message.fragments.filter((fragment) => fragment.hasOwnProperty("emoticon"))) {
				emotes[fragment.text] = {
					id: fragment.emoticon.emoticon_id,
					src: `//static-cdn.jtvnw.net/emoticons/v1/${fragment.emoticon.emoticon_id}/3.0`
				};
			}
		}

		return emotes;
	};

	// Get FFZ emotes.
	async function Get_FFZ_Emotes() {
		let emotes = {};

		// Global.
		let config = {
			baseURL: "https://api.frankerfacez.com/v1/",
			url: "set/global",
			method: "get"
		};
		await axios.request(config)
			.then((res) => {
				// Set.
				for (const set of Object.values(res.data.sets)) {
					// Emoticons.
					for (const emoticon of set.emoticons) {
						emotes[emoticon.name] = {
							id: emoticon.id,
							src: Object.entries(emoticon.urls).sort((a, b) => (parseInt(a[0], 10) - parseInt(b[0], 10)))[Object.entries(emoticon.urls).length - 1][1]
						};
					}
				}
			})
			.catch((error) => {});

		// Channel.
		config.url = `room/id/${video.user_id}`;
		await axios.request(config)
			.then((res) => {
				// Set.
				for (const set of Object.values(res.data.sets)) {
					// Emoticons.
					for (const emoticon of set.emoticons) {
						emotes[emoticon.name] = {
							id: emoticon.id,
							src: Object.entries(emoticon.urls).sort((a, b) => (parseInt(a[0], 10) - parseInt(b[0], 10)))[Object.entries(emoticon.urls).length - 1][1]
						};
					}
				}
			})
			.catch((error) => {});

		return emotes;
	};

	// Get BTTV emotes.
	async function Get_BTTV_Emotes() {
		let emotes = {};

		// Global.
		let config = {
			baseURL: "https://api.betterttv.net/2/",
			url: "emotes",
			method: "get"
		};
		await axios.request(config)
			.then((res) => {
				// Emotes.
				for (const emote of res.data.emotes) {
					// Emote.
					emotes[emote.code] = {
						id: emote.id,
						src: `//cdn.betterttv.net/emote/${emote.id}/3x`
					};
				}
			})
			.catch((error) => {});

		// Channel.
		config.url = `channels/${video.user_name.toLowerCase()}`;
		await axios.request(config)
			.then((res) => {
				// Emotes.
				for (const emote of res.data.emotes) {
					// Emote.
					emotes[emote.code] = {
						id: emote.id,
						src: `//cdn.betterttv.net/emote/${emote.id}/3x`
					};
				}
			})
			.catch((error) => {});

		return emotes;
	};

	// Get emotes.
	async function Get_Emotes() {
		if (video && comments) {
			let emotes = await Promise.all([Get_Twitch_Emotes(), Get_FFZ_Emotes(), Get_BTTV_Emotes()]);
			emotes = {
				"Twitch": emotes[0],
				"FFZ": emotes[1],
				"BTTV": emotes[2]
			};
			Set_emotes(emotes);
		}
		else {
			Set_emotes();
		}
	};

	// Get data.
	function Get_Data() {
		if (comments && emotes) {
			let counter = {};

			// Find the number of comments per second.
			for (let comment of comments) {
				let has_emote = false;
				// Twitch emote.
				if (comment.message.emoticons) {
					has_emote = true;
				}
				// FFZ emote.
				if (!has_emote) {
					for (let emote of Object.keys(emotes.FFZ)) {
						if (comment.message.body.match(emote)) {
							has_emote = true;
							break;
						}
					}
				}
				// BTTV emote.
				if (!has_emote) {
					for (let emote of Object.keys(emotes.FFZ)) {
						if (comment.message.body.match(emote)) {
							has_emote = true;
							break;
						}
					}
				}
				if (has_emote) {
					counter[Math.floor(comment.content_offset_seconds)] = (counter[Math.floor(comment.content_offset_seconds)] || 0) + 1;
				}
			}

			// Transform data.

			// High-pass filtering.
			let emote_density_pmf = {};
			for (let v of Object.values(counter)) {
				// Find the distribution of messages with emotes per second.
				emote_density_pmf[v] = (emote_density_pmf[v] || 0) + 1;
			}

			console.log("Emote density pmf:", emote_density_pmf);

			let emote_density_total = Object.values(emote_density_pmf).reduce((a, e) => (a + e), 0);
			let percentile = 0.95;
			let num_items_hidden = Math.floor(emote_density_total*percentile);
			let min_emote_density = 0;
			let num_items = 0;
			for (let item of Object.entries(emote_density_pmf).sort((a, b) => a[0] - b[0])) {
				if (num_items <= num_items_hidden) {
					min_emote_density = parseInt(item[0], 0);
					num_items += item[1];
				}
				else {
					break;
				}
			}

			console.log(`Minimum emote density: ${min_emote_density}`, `\nThreshold for # of hidden items: ${num_items_hidden}`, `\n# of hidden items: ${num_items}`);

			for (let item of Object.entries(counter)) {
				if (item[1] < min_emote_density) {
					delete counter[item[0]];
				}
			}

			// Curve flooring + low-pass filtering.
			for (let i of Object.keys(counter).map((e) => parseInt(e, 10))) {
				// Insert 0's before and after datapoints if needed. Used to properly low-pass filter and floor the line graph.
				if (0 <= i - 1) {
					if (!counter[i - 1]) {
						counter[i - 1] = 0;
					}
				}
				if (i + 1 <= video.duration_seconds) {
					if (!counter[i + 1]) {
						counter[i + 1] = 0;
					}
				}
			}

			let data = {
				datasets: [
					{
						data: Object.entries(counter).map((e) => ({x: e[0], y: e[1]}))
					}
				]
			};

			Set_data(data);
		}
		else {
			Set_data({
				datasets: [
					{
						data: []
					}
				]
			});
		}
	};

	// Format h:mm:ss into seconds.
	function Unformat_Time(h_mm_ss) {
		return h_mm_ss.split(RegExp(/\D/)).slice(0, -1).reduce((seconds, e, i, a) => seconds + parseInt(e, 10)*(Math.pow(60, a.length - 1 - i)), 0);
	}

	// Format seconds into h:mm:ss.
	function Format_Time(seconds) {
		seconds = Math.floor(seconds);
		let ss = (seconds % 60).toString().padStart(2, "0");
		let mm = (Math.floor(seconds/60) % 60).toString().padStart(2, "0");
		let h = (Math.floor(seconds/(60 * 60))).toString().padStart(1, "0");
		return `${h}:${mm}:${ss}`;
	}

	/*
		Effects.
	*/
	// Create the chart.
	useEffect(() => {Create_Chart()}, []);

	// Update chart.
	useEffect(() => {Update_Chart()}, [video, data]);

	// Load the video.
	useEffect(() => {Get_Video()}, [video_id]);

	// Change the title.
	useEffect(() => {document.title = video ? `${video.user_name} - ${video.title}` : "Emote Only Mode"}, [video]);

	// Load the comments.
	useEffect(() => {Get_Comments()}, [video]);

	// Load the emotes.
	useEffect(() => {Get_Emotes()}, [comments]);

	// Generate the data.
	useEffect(() => {Get_Data()}, [emotes]);


	/*
		DOM.
	*/
	return (
		<div className="Video">
			<div className="Content">
				<div className="Player">
					{video && <TwitchPlayer ref={player} className="Embed" url={video.url} playing controls height="" width=""/>}
				</div>

				<div className="Timeline">
					<canvas ref={ctx}></canvas>
				</div>
			</div>

			<div className="Chat">

			</div>
		</div>
	);
};

export default Video;